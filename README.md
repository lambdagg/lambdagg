## Hello there 👋

[![GitLab](https://img.shields.io/static/v1?label=&message=lambdagg&logo=gitlab&logoColor=ffffff&color=5D5D5D&style=flat)](https://github.com/lambdagg)
[![Discord](https://img.shields.io/static/v1?label=&message=lambdagg%234887&logo=discord&logoColor=ffffff&color=5D5D5D&style=flat)](https://discord.com/app)
[![Twitter](https://img.shields.io/static/v1?label=&message=@lambdagg&logo=twitter&logoColor=ffffff&color=5D5D5D&style=flat)](https://twitter.com/lambdagg)
[![E-mail](https://img.shields.io/static/v1?label=&message=lambdagg@tuta.io&color=5D5D5D&style=flat)](mailto:lambdagg@tuta.io)
<!--[![E-mail](https://img.shields.io/static/v1?label=&message=lambda@stardustenterprises.fr&color=5D5D5D&style=flat)](mailto:lambda@stardustenterprises.fr)
[![E-mail](https://img.shields.io/static/v1?label=&message=lambda@jikt.im&color=5D5D5D&style=flat)](mailto:lambda@jikt.im)-->

stupid 17-years-old tech enthusiast. they/them\
always trying to learn new things!


![Stats](https://github-readme-stats.vercel.app/api/wakatime?username=lambdagg&theme=dark&layout=compact)


### ✨ About me
I started programming at the age of 11. I started with PHP, along with the front-end web stuff. Slowly, I discovered
more and more technologies, starting off by C#, then Java, TypeScript, and most recently Kotlin and Rust.\
I'm most of the time either programming stuff or [playing some games](https://steamcommunity.com/id/lambdagg/). I may
also be cooking or tinkering.\
I love contributing to open-source projects and making some on my own.

I write stupid software as a part of [@jiktim](https://github.com/jiktim). jiktim. everything jik.\
I also contribute to [@stardust-enterprises](https://github.com/stardust-enterprises) from times to times.


#### 🤔 Abilities
- Object-oriented languages such as C#, Java, TypeScript & Kotlin for their accessibility.
- Python for fast-scripting instead of just using batch or bash.
- Front-end languages (HTML, CSS, JS). Still working on the style side though.
- A few hundreds of coffees spent on trying out stuff using Gradle and the Kotlin DSL.
- Strong experience with the Bukkit and Bungee APIs (Minecraft), trying out Minestom from times to times.


### 👯 Collaborating
I'm looking forward to collaborating with whoever has well defined ideas ready to be put in motion together!
